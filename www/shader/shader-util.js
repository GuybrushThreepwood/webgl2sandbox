
var shaderMap = [];

function clearShaderMap() {
	
	var len = shaderMap.length;
	
	// loop over the array and splice
	while( i-- ) {
		
		var currShader = shaderMap[i];
		
		if( currShader.program === programId ) {
			
			if( currShader.vertexShader.shaderId != -1 &&
				gl.isShader(currShader.vertexShader.shaderId) )
			{
				gl.detachShader(programId, currShader.vertexShader.shaderId);
				gl.deleteShader(currShader.vertexShader.shaderId);
			}

			if( currShader.fragmentShader.shaderId != -1 &&
				gl.isShader(currShader.fragmentShader.shaderId) )
			{
				gl.detachShader(programId, currShader.fragmentShader.shaderId);
				gl.deleteShader(currShader.fragmentShader.shaderId);
			}

			if (currShader.geometryShader.shaderId != -1 &&
				gl.isShader(currShader.geometryShader.shaderId))
			{
				gl.detachShader(programId, currShader.geometryShader.shaderId);
				gl.deleteShader(currShader.geometryShader.shaderId);
			}
			
			if( gl.isProgram( programId ) ) {
				gl.deleteProgram( programId );
			}	
			
			shaderMap.splice(i, 1);
		}
	}

}

function removeShaderProgram( gl, programId ) {
	
	// find and remove the shader program
	for( var i=0; i < shaderMap.length; ++i ) {
		
		var currShader = shaderMap[i];
		
		if( currShader.program === programId ) {
			
			// remove all attachments
			if( currShader.vertexShader.shaderId != -1 &&
				gl.isShader(currShader.vertexShader.shaderId) )
			{
				gl.detachShader(programId, currShader.vertexShader.shaderId);
				gl.deleteShader(currShader.vertexShader.shaderId);
			}

			if( currShader.fragmentShader.shaderId != -1 &&
				gl.isShader(currShader.fragmentShader.shaderId) )
			{
				gl.detachShader(programId, currShader.fragmentShader.shaderId);
				gl.deleteShader(currShader.fragmentShader.shaderId);
			}

			if (currShader.geometryShader.shaderId != -1 &&
				gl.isShader(currShader.geometryShader.shaderId))
			{
				gl.detachShader(programId, currShader.geometryShader.shaderId);
				gl.deleteShader(currShader.geometryShader.shaderId);
			}
			
			if( gl.isProgram( programId ) ) {
				gl.deleteProgram( programId );
			}	
			
			shaderMap.splice(i, 1);
			
			return;
		}
	}	
}

function printInfoLog( gl, objectId ) {
	
	var isValid = true;
	var infoLog;
	var isShader = true;
	
	// determine if it's a shader or a program
	if ( !(objectId instanceof WebGLShader) )
		isShader = false;
	
	// determine if object is a shader or a program
	if( isShader ) {
		isValid = gl.getShaderParameter( objectId, gl.COMPILE_STATUS );
	} else {
		isValid = gl.getProgramParameter( objectId, gl.VALIDATE_STATUS );
	}		
	
	// something is wrong with the object
	if( !isValid ) {
		if( isShader ) {
			// shader errors
			infoLog = gl.getShaderInfoLog( objectId );
			console.log( 'SHADER Info Log:' );
		} else {
			// program errors
			infoLog = gl.getProgramInfoLog( objectId );
			console.log( 'PROGRAM Info Log:' );			
		}
		
		console.log(infoLog);
	}
	
	return { 
		valid:isValid,
		log:infoLog
	};
}

function loadShader( gl, shaderType, shaderString ) {
	
	// empty shader string is no good
	if( !shaderString ||
		shaderString.length === 0 ) {
			
		console.log( 'loadShader() ERROR: empty string' );
		return -1;
	}
	
	// check supported shader types
	if( shaderType != gl.VERTEX_SHADER &&
		shaderType != gl.FRAGMENT_SHADER &&
		shaderType != gl.GEOMETRY_SHADER ) {
		console.log( 'loadShader() ERROR: invalid shader type' );
		return -1;		
	}

	// create a new shader object
	var shaderId;
	shaderId = gl.createShader(shaderType);
	
	// attach the source and compile
	gl.shaderSource( shaderId, shaderString );
	gl.compileShader( shaderId );
	
	var infoData = printInfoLog( gl, shaderId );
	
	if(!infoData.valid) {
		return { 
			shaderId:-1,
			infoLog:infoData.log
		};
	}

	return { 
		shaderId:shaderId,
		infoLog:infoData.log
	};
}

function loadShadersForProgram( gl, vertexShader, fragmentShader, geometryShader ) {
	var vsId = {};
	var fsId = {};
	var gsId = {};
		
	// load a vertex shader
	if( vertexShader &&
		vertexShader.length != 0 ) {	
		vsId = loadShader( gl, gl.VERTEX_SHADER, vertexShader );
	} else {
		vsId.shaderId = -1;
	}
	
	// load a fragment shader
	if( fragmentShader &&
		fragmentShader.length != 0 ) {	
		fsId = loadShader( gl, gl.FRAGMENT_SHADER, fragmentShader );
	} else {
		fsId.shaderId = -1;
	}

	// load a geometry shader
	if( geometryShader &&
		geometryShader.length != 0 ) {	
		gsId = loadShader( gl, gl.GEOMETRY_SHADER, geometryShader );
	} else {
		gsId.shaderId = -1;
	}
	
	// valid ids to load into a program
	if( vsId.shaderId != -1 ||
		fsId.shaderId != -1 ||
		gsId.shaderId != -1 ) {
	
		var programId = -1;
		programId = gl.createProgram();
		
		if( vsId.shaderId != -1 )
			gl.attachShader( programId, vsId.shaderId );

		if( fsId.shaderId != -1 )
			gl.attachShader( programId, fsId.shaderId );

		if( gsId.shaderId != -1 )
			gl.attachShader( programId, gsId.shaderId );
		
		gl.linkProgram( programId );
		
		var isLinked = false;
		isLinked = gl.getProgramParameter( programId, gl.LINK_STATUS );
		
		// DEBUG **********
		var validationStatus;
		gl.validateProgram(programId);
		printInfoLog(gl, programId);
		// ****************
		
		if(!isLinked){
			return	{
				programId:-1,
				vsData:vsId,
				fsData:fsId,
				gsData:gsId
			}
		}
		
		var completeShader = {
			program: programId,
			vertexShader: vsId,
			fragmentShader: fsId,
			geometryShader: gsId,			
		};

		shaderMap.push(completeShader);
		
		return {
			programId:programId,
			vsData:vsId,
			fsData:fsId,
			gsData:gsId
		};
	} 
	
	return {
		programId:-1,
		vsData:vsId,
		fsData:fsId,
		gsData:gsId
	};
}
