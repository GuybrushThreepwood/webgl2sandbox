
var ShaderUniformName = {
	ModelViewProjectionMatrix : 0,
	ModelViewMatrix : 1,
	ViewMatrix : 2,
	NormalMatrix : 3,
	VertexColour : 4,

	TexSampler0 : 5,
	TexSampler1 : 6,
	TexSampler2 : 7,
	TexSampler3 : 8	
};

var ShaderUniformType = {

	UniformType_Int1 : 0,
	UniformType_Int2 : 1,
	UniformType_Int3 : 2,
	UniformType_Int4 : 3,

	UniformType_Float1 : 4,
	UniformType_Float2 : 5,
	UniformType_Float3 : 6,
	UniformType_Float4 : 7,	

	UniformType_Vec2 : 8,
	UniformType_Vec3 : 9,
	UniformType_Vec4 : 10,

	UniformType_Mat4 : 11,
};

var AllUniforms = [
	{
		uniformIdent: ShaderUniformName.ModelViewProjectionMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ModelViewProjectionMatrix"
	},
	
	{
		uniformIdent: ShaderUniformName.ModelViewMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ModelViewMatrix"
	},	
	
	{
		uniformIdent: ShaderUniformName.ViewMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ViewMatrix"
	},		
	
	{
		uniformIdent: ShaderUniformName.NormalMatrix,
		uniformType: ShaderUniformType.UniformType_Vec4,
		uniformLocation: -1,
		uniformString: "ogl_NormalMatrix"
	},
	
	{
		uniformIdent: ShaderUniformName.VertexColour,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_VertexColour"
	},	


	{
		uniformIdent: ShaderUniformName.TexSampler0,
		uniformType: ShaderUniformType.UniformType_Int1,
		uniformLocation: -1,
		uniformString: "ogl_TexUnit0"
	},	
	{
		uniformIdent: ShaderUniformName.TexSampler1,
		uniformType: ShaderUniformType.UniformType_Int1,
		uniformLocation: -1,
		uniformString: "ogl_TexUnit1"
	},
	{
		uniformIdent: ShaderUniformName.TexSampler2,
		uniformType: ShaderUniformType.UniformType_Int1,
		uniformLocation: -1,
		uniformString: "ogl_TexUnit2"
	},
	{
		uniformIdent: ShaderUniformName.TexSampler3,
		uniformType: ShaderUniformType.UniformType_Int1,
		uniformLocation: -1,
		uniformString: "ogl_TexUnit3"
	},	
];

function SetUniform ( uniformName, value ) {
	var u;
	
	if( (typeof uniformName === 'string' ) ) {
		u = this.uniformMap[uniformName];
	} else {
		u = this.uniformMap[uniformName.toString()];
	}
	
	if( u === null ||
		u === undefined ) {
		return;
	}
		
	var loc = u.uniformLocation;
	var type = u.uniformType;
		
	if( loc != -1 ) {
		if( type == ShaderUniformType.UniformType_Int1 ) {
			gl.uniform1i( loc, value );		
		} else if( type == ShaderUniformType.UniformType_Int2 ) {
			gl.uniform2iv( loc, value.elements );		
		} else if( type == ShaderUniformType.UniformType_Int3 ) {
			gl.uniform3iv( loc, value.elements );		
		}  else if( type == ShaderUniformType.UniformType_Int4 ) {
			gl.uniform4iv( loc, value.elements );		
		} else if( type == ShaderUniformType.UniformType_Float1 ) {
			gl.uniform1fv( loc, value );		
		} else if( type == ShaderUniformType.UniformType_Float2 ) {
			gl.uniform2fv( loc, value.elements );		
		} else if( type == ShaderUniformType.UniformType_Float3 ) {
			gl.uniform3fv( loc, value.elements );		
		} else if( type == ShaderUniformType.UniformType_Float4 ) {
			gl.uniform4fv( loc, value.elements );		
		} else if( type == ShaderUniformType.UniformType_Vec2 ) {
			gl.uniform2fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Vec3 ) {
			gl.uniform3fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Vec4 ) {
			gl.uniform4fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Mat4 ) {
			gl.uniformMatrix4fv( loc, gl.GL_FALSE, value.elements );
		}				
	}
}

function Clear() {
	if( this.programId != -1 ) {
		this.isValid = false;
		this.previousProgramId = -1;

		removeShaderProgram( gl, this.programId );
	}
}

function Bind() {
	// bind the shader program
	if( this.programId != -1 ) {
		this.previousProgramId = gl.getParameter( gl.CURRENT_PROGRAM );
		gl.useProgram(this.programId);
	}
}
	
function UnBind () {
	// unbind and return to previous program if it existed
	if( this.previousProgramId != -1 ) {
		gl.useProgram(this.previousProgramId);
	} else {
		gl.useProgram(0);
	}
}

function GetInfoLogs() {
	return this.infoLogs;
}

function IsValid() {
	return this.isValid;
}

function Shader(gl, vertexShader, fragmentShader, geometryShader) {
	this.gl = gl;
	this.programId = -1;
	this.previousProgramId = 1;		
	this.uniformMap = {};		
	this.isValid = false;
	this.infoLogs = {};

// assign object funcs
	this.SetUniform = SetUniform;
	this.Clear = Clear;
	this.Bind = Bind;
	this.UnBind = UnBind;
	this.IsValid = IsValid;
	this.GetInfoLogs = GetInfoLogs;

	// create the program
	var shaderData = loadShadersForProgram( gl, vertexShader, fragmentShader, geometryShader );
	
	if( shaderData.programId != -1 ) {
		this.programId = shaderData.programId;

		// cache uniforms
		if( this.programId != -1 ) {
			for( var i=0; i < AllUniforms.length; ++i ) {
			
				var u = AllUniforms[i];
				u.uniformLocation = gl.getUniformLocation( this.programId, u.uniformString );

				// add to the map array
				
				this.uniformMap[u.uniformString] = u;
				this.uniformMap[u.uniformIdent.toString()] = u;
			}
		}

		this.isValid = true;
	} else {
		this.isValid = false;

		this.infoLogs = shaderData;
	}

}
