import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ToastController} from 'ionic-angular';

// external values
declare function webGL2CreateContext( elementName );
declare function registerRenderCallbacks( callerObject, updateCB, drawCB, resizeCB );
declare var gl : any;
declare var glm : any;
declare var Shader : any;

@Component({
  templateUrl: 'build/pages/home/home.html'
})

export class HomePage {
	depthState : boolean;
	depthFunc : any;
	depthFuncOptions : any;

	blendState : boolean;
	blendSrc : any;
	blendDest : any;
	
	blendSrcOptions : any;
	blendDestOptions : any;

	fieldOfView : any;
	bgRed : number;
	bgGreen : number;
	bgBlue : number;
  
	vertexShader : string;
	fragmentShader : string;

	vertexShaderInfo = `No Errors
	`;
	fragmentShaderInfo = `No Errors
	`;

	shaderObject:any;

	texImage0 : any;
	texImage1 : any;

	texUnit0;
	texUnit1;
	textureLoaded = 0;

	constructor(private navCtrl: NavController, private toastCtrl: ToastController) {
  
	}

	ngOnInit() {
		console.log('ngOnInit called');

		var validContext = webGL2CreateContext( 'outputCanvas' );
	
		if( !validContext ) {
			let toast = this.toastCtrl.create({
				message: `WebGL2 context not created. Check your browser is up to date and webgl2 is enabled.\n
				Chrome requires *WebGL 2.0 Prototype* enabled in about:flags.\n
				Firefox requires *webgl.enable-prototype-webgl2* enabled in about:config.\n`,
				position: 'middle',
				showCloseButton : true
			});

			toast.present();
			return;
		}
		
		this.depthState = true;
		this.depthFunc = gl.LESS;

		this.blendState = false;
		this.blendSrc = gl.SRC_ALPHA;
		this.blendDest = gl.ONE_MINUS_SRC_ALPHA;

		this.fieldOfView = 45;
		this.bgRed = 128;
		this.bgGreen = 128;
		this.bgBlue = 128;

		this.texImage0 = "tex0.png";
		this.texImage1 = "tex1.png";

		this.texUnit0;
		this.texUnit1;

		this.vertexShader = `#version 300 es

		layout(location=0) in vec3 attr_v;
		layout(location=1) in vec2 attr_uv;

		out vec2 tu0;
		uniform mat4 ogl_ModelViewProjectionMatrix;

		void main(void) {
			tu0 = attr_uv;
			gl_Position = ogl_ModelViewProjectionMatrix * vec4(attr_v, 1.0);
		}
		`;
		this.fragmentShader = `#version 300 es
		precision mediump float;

		uniform sampler2D ogl_TexUnit0;
		uniform sampler2D ogl_TexUnit1;
		uniform sampler2D ogl_TexUnit2;
		uniform sampler2D ogl_TexUnit3;
		
		in vec2 tu0;

		out vec4 fragColour;
		
		void main(void) {

			vec4 firstTex = texture(ogl_TexUnit0, tu0.xy);
			vec4 secondTex = texture(ogl_TexUnit1, tu0.xy);

			fragColour = firstTex * secondTex;
		}
		`;	  

		this.depthFuncOptions = [
			{
				name:"NEVER",
				value:gl.NEVER
			},
			{
				name:"ALWAYS",
				value:gl.ALWAYS
			},
			{
				name:"LESS",
				value:gl.LESS
			},
			{
				name:"LEQUAL",
				value:gl.LEQUAL
			},
			{
				name:"EQUAL",
				value:gl.EQUAL
			},
			{
				name:"GREATER",
				value:gl.GREATER
			},
			{
				name:"GEQUAL",
				value:gl.GEQUAL
			},
			{
				name:"NOTEQUAL",
				value:gl.NOTEQUAL
			}	
		];	

		this.blendSrcOptions = [
			{
				name:"ZERO",
				value:gl.ZERO
			},
			{
				name:"ONE",
				value:gl.ONE
			},
			{
				name:"DST_COLOR",
				value:gl.DST_COLOR
			},
			{
				name:"ONE_MINUS_DST_COLOR",
				value:gl.ONE_MINUS_DST_COLOR
			},
			{
				name:"SRC_ALPHA_SATURATE",
				value:gl.SRC_ALPHA_SATURATE
			},
			{
				name:"SRC_ALPHA",
				value:gl.SRC_ALPHA
			},
			{
				name:"ONE_MINUS_SRC_ALPHA",
				value:gl.ONE_MINUS_SRC_ALPHA
			},
			{
				name:"DST_ALPHA",
				value:gl.DST_ALPHA
			},
			{
				name:"ONE_MINUS_DST_ALPHA",
				value:gl.ONE_MINUS_DST_ALPHA
			}
		];

		this.blendDestOptions = [
			{
				name:"ZERO",
				value:gl.ZERO
			},
			{
				name:"ONE",
				value:gl.ONE
			},
			{
				name:"SRC_COLOR",
				value:gl.SRC_COLOR
			},
			{
				name:"ONE_MINUS_SRC_COLOR",
				value:gl.ONE_MINUS_SRC_COLOR
			},
			{
				name:"SRC_ALPHA",
				value:gl.SRC_ALPHA
			},
			{
				name:"ONE_MINUS_SRC_ALPHA",
				value:gl.ONE_MINUS_SRC_ALPHA
			},
			{
				name:"DST_ALPHA",
				value:gl.DST_ALPHA
			},
			{
				name:"ONE_MINUS_DST_ALPHA",
				value:gl.ONE_MINUS_DST_ALPHA
			}
		];
	}	

	resizeView() {

			var canvasParent = document.getElementById('canvasParent');
			if( canvasParent.clientWidth == 0 ||
				canvasParent.clientHeight == 0 ) {
				return;
			}
			/*console.log( canvasParent.clientWidth );
			var renderArea = document.getElementById('renderArea');
			var widthToHeight = 4 / 3;
			var newWidth = window.innerWidth;
			var newHeight = window.innerHeight;
			var newWidthToHeight = newWidth / newHeight;

			if (newWidthToHeight > widthToHeight) {
				newWidth = newHeight * widthToHeight;
				renderArea.style.height = newHeight + 'px';
				renderArea.style.width = newWidth + 'px';
			} else {
				newHeight = newWidth / widthToHeight;
				renderArea.style.width = newWidth + 'px';
				renderArea.style.height = newHeight + 'px';
			}

			renderArea.style.marginTop = (-newHeight / 2) + 'px';
			renderArea.style.marginLeft = (-newWidth / 2) + 'px';
			*/
			var outputCanvas = <HTMLCanvasElement>document.getElementById('outputCanvas');

			if( canvasParent.clientWidth < canvasParent.clientHeight) {
				outputCanvas.width = canvasParent.clientWidth;
				outputCanvas.height = canvasParent.clientWidth;
			} else {
				outputCanvas.width = canvasParent.clientHeight;
				outputCanvas.height = canvasParent.clientHeight;
			}

			gl.viewport(0, 0, outputCanvas.width, outputCanvas.height);
	}	
	
	initShaders() {
			
		if( this.shaderObject != null ||
			this.shaderObject != undefined ) {
			this.shaderObject.Clear();
		}

		this.shaderObject = new Shader( gl, this.vertexShader, this.fragmentShader, "" );

		if( !this.shaderObject.IsValid() ) {
			var logData = this.shaderObject.GetInfoLogs();

			if( logData.vsData.infoLog != null &&
				logData.vsData.infoLog.length != 0 )
			{
				this.vertexShaderInfo = logData.vsData.infoLog;
			} else {
				this.vertexShaderInfo = `No Errors`;
			}

			if( logData.fsData.infoLog != null &&
				logData.fsData.infoLog.length != 0 )
			{
				this.fragmentShaderInfo = logData.fsData.infoLog;
			} else {
				this.fragmentShaderInfo = `No Errors`;
			}	

		} else {
			this.vertexShaderInfo = `No Errors
			`;
			this.fragmentShaderInfo = `No Errors
			`;
		}
	}	

	handleLoadedTexture(texture:any, unit:number) {
		var textureUnit : number = gl.GL_TEXTURE0;
		
		gl.activeTexture ( textureUnit + (unit) );
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.bindTexture(gl.TEXTURE_2D, null);

		this.textureLoaded++;
	}

	initTexture() {
		var callerObj = this;
		this.textureLoaded = 0;

		// clear textures
		if( this.texUnit0 != null &&
			this.texUnit0 != undefined ) {
			gl.deleteTexture( this.texUnit0 );
		}
		if( this.texUnit1 != null &&
			this.texUnit1 != undefined ) {
	   		gl.deleteTexture( this.texUnit1 );
		}

		// tex unit 0		
		this.texUnit0 = gl.createTexture();
		this.texUnit0.image = new Image();
			
		var texIn0 = this.texUnit0;
		
		this.texUnit0.image.onload = function () {
			callerObj.handleLoadedTexture(texIn0, 0)
		}
		this.texUnit0.image.src = this.texImage0;

		// tex unit 1
		this.texUnit1 = gl.createTexture();
		this.texUnit1.image = new Image();

		var texIn1 = this.texUnit1;

		this.texUnit1.image.onload = function () {
			callerObj.handleLoadedTexture(texIn1, 1)
		}
		this.texUnit1.image.src = this.texImage1;
	}

	ngAfterViewInit() {

		var	SHADERATTRIB_VERT = 0;
		var	SHADERATTRIB_TEXCOORDS = 1;
		var	SHADERATTRIB_COLOUR = 2;
		
		var yRot = 0;

		var mvMatrix = glm.mat4(1.0);
		var projMatrix = glm.mat4(1.0);
		var vertexArrayObj1;
		
		var cubeVertexPositionBuffer;
		var cubeVertexTextureCoordBuffer;
		var cubeVertexIndexBuffer;

		function initBuffers(callerObject:any) {
			vertexArrayObj1 = gl.createVertexArray();
			gl.bindVertexArray(vertexArrayObj1);
			
			gl.enableVertexAttribArray(SHADERATTRIB_VERT);				
			gl.enableVertexAttribArray(SHADERATTRIB_TEXCOORDS);	

			// vertex buffer
			cubeVertexPositionBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
			var vertices = [
				// Front face
				-1.0, -1.0,  1.0,
				1.0, -1.0,  1.0,
				1.0,  1.0,  1.0,
				-1.0,  1.0,  1.0,

				// Back face
				-1.0, -1.0, -1.0,
				-1.0,  1.0, -1.0,
				1.0,  1.0, -1.0,
				1.0, -1.0, -1.0,

				// Top face
				-1.0,  1.0, -1.0,
				-1.0,  1.0,  1.0,
				1.0,  1.0,  1.0,
				1.0,  1.0, -1.0,

				// Bottom face
				-1.0, -1.0, -1.0,
				1.0, -1.0, -1.0,
				1.0, -1.0,  1.0,
				-1.0, -1.0,  1.0,

				// Right face
				1.0, -1.0, -1.0,
				1.0,  1.0, -1.0,
				1.0,  1.0,  1.0,
				1.0, -1.0,  1.0,

				// Left face
				-1.0, -1.0, -1.0,
				-1.0, -1.0,  1.0,
				-1.0,  1.0,  1.0,
				-1.0,  1.0, -1.0,
			];				
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
			cubeVertexPositionBuffer.itemSize = 3;
			cubeVertexPositionBuffer.numItems = 24;
			
			// texture coords
			cubeVertexTextureCoordBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
			var textureCoords = [
			// Front face
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,

			// Back face
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,

			// Top face
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,

			// Bottom face
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,

			// Right face
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,

			// Left face
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
			cubeVertexTextureCoordBuffer.itemSize = 2;
			cubeVertexTextureCoordBuffer.numItems = 24;

			// index buffer
			cubeVertexIndexBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
			var cubeVertexIndices = [
				0, 1, 2,      0, 2, 3,    // Front face
				4, 5, 6,      4, 6, 7,    // Back face
				8, 9, 10,     8, 10, 11,  // Top face
				12, 13, 14,   12, 14, 15, // Bottom face
				16, 17, 18,   16, 18, 19, // Right face
				20, 21, 22,   20, 22, 23  // Left face
			];
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
			cubeVertexIndexBuffer.itemSize = 1;
			cubeVertexIndexBuffer.numItems = 36;

			// set attribs
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
			gl.vertexAttribPointer(SHADERATTRIB_VERT, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
			
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
			gl.vertexAttribPointer(SHADERATTRIB_TEXCOORDS, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// done 
			gl.bindVertexArray(null);
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);
		}

		function drawScene(callerObject:any) {
			gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

			projMatrix = glm.perspective(glm.radians(callerObject.fieldOfView), gl.drawingBufferWidth / gl.drawingBufferHeight, 0.1, 100.0);

			mvMatrix = glm.mat4(1.0);

			if( callerObject.shaderObject.IsValid() ) 
			{
				callerObject.shaderObject.Bind();
					if( callerObject.textureLoaded == 2 ) {
						gl.activeTexture(gl.TEXTURE1);
						gl.bindTexture(gl.TEXTURE_2D, callerObject.texUnit1);
						callerObject.shaderObject.SetUniform( "ogl_TexUnit1", 1 );

						gl.activeTexture(gl.TEXTURE0);
						gl.bindTexture(gl.TEXTURE_2D, callerObject.texUnit0);
						callerObject.shaderObject.SetUniform( "ogl_TexUnit0", 0 );
					} else {
						gl.activeTexture(gl.TEXTURE1);
						gl.bindTexture(gl.TEXTURE_2D, null);

						gl.activeTexture(gl.TEXTURE0);
						gl.bindTexture(gl.TEXTURE_2D, null);
					}
					

					gl.bindVertexArray(vertexArrayObj1);
						mvMatrix = glm.translate(mvMatrix, glm.vec3(0.0, 0.0, -7.0));
						mvMatrix = glm.rotate(mvMatrix, yRot, glm.vec3(0.0, 1.0, 0.0));
						
						callerObject.shaderObject.SetUniform( "ogl_ModelViewProjectionMatrix", projMatrix['*'](mvMatrix) );
						
						gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
					gl.bindVertexArray(null); 
				callerObject.shaderObject.UnBind();
			}
		}

		function updateScene(callerObject:any, deltaTime:number) {

			yRot += (1.0*deltaTime);
		}	

// main boot
		this.initShaders();
			
		initBuffers(this);

		this.initTexture();

		this.updateSettings();

		registerRenderCallbacks( this, drawScene, updateScene, this.resizeView );
  	}

	ngAfterContentChecked() {
		this.resizeView();
	}

	ngOnDestroy() {
		// cleanup
		if( this.texUnit0 != null &&
			this.texUnit0 != undefined ) {
			gl.deleteTexture( this.texUnit0 );
		}
		if( this.texUnit1 != null &&
			this.texUnit1 != undefined ) {
	   		gl.deleteTexture( this.texUnit1 );
		}
	}

	updateSettings() {

		gl.clearColor(this.bgRed/255.0, this.bgGreen/255.0, this.bgBlue/255.0, 1.0);
	
	// depth
		if(this.depthState) {
			gl.enable(gl.DEPTH_TEST);
		} else {
			gl.disable(gl.DEPTH_TEST);
		}	

		gl.depthFunc(this.depthFunc);

	// blend
		if(this.blendState) {
			gl.enable(gl.BLEND );
		} else {
			gl.disable(gl.BLEND );
		}	

		gl.blendFunc(this.blendSrc, this.blendDest);
  	}

	updateShader() {
		this.initShaders();
	}

	updateTextures() {
		this.initTexture();
	}

	renderImage(file, whichUnit){
		var reader = new FileReader();
		var textureUnit = whichUnit;
		var callerObject = this;
		reader.onload = function(event){
			if( reader.result != null &&
				reader.result.length > 0  ) {
				var imageURL = reader.result;
				
				if( textureUnit == 0 ) {
					callerObject.texImage0 = imageURL;
				} else {
					callerObject.texImage1 = imageURL;
				}
				callerObject.initTexture();
			}
		}
    
		//when the file is read it triggers the onload event above.
		reader.readAsDataURL(file);
	}

	loadImage(event, whichUnit) {
		var element = event.currentTarget;
		var fileIn = element.files[0];

		this.renderImage(fileIn, whichUnit);
	}

	resetImage(whichUnit) {
		var lookUpElement = "Texture" + whichUnit + "Form";
		var textureForm = <HTMLFormElement>document.getElementById(lookUpElement);
		textureForm.reset();
		
		if( whichUnit == 0 ) {
			this.texImage0 = "tex0.png";
		} else {
			this.texImage1 = "tex1.png";
		}
		this.initTexture();
	}
}
